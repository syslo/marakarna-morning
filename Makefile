RESULTS = $(patsubst %.tex, %.pdf, $(wildcard den*.tex))

all: ${RESULTS}

redo: clean-all all clean-tmp

clean-tmp:
	rm -f *.aux *.log

clean-all:
	rm -f *.pdf *.aux *.log

den%.pdf: den%.tex kostra.tex
	pdflatex $<

